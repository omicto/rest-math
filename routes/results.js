const express = require('express');
const router = express.Router();
const controller = require('../controllers/result/resultsController');

router
.get('/:a/:b', controller.sum)
.post('/:a/:b', controller.multiplication)
.put('/:a/:b', controller.division)
.delete('/:a/:b', controller.substraction);

module.exports = router;
