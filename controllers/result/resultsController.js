const express = require('express');

function sum(req, res){
    twoParameterOperation(req, res, (a, b) => a + b);
}

function substraction(req, res){
    twoParameterOperation(req, res, (a, b) => a - b);
}

function multiplication(req, res){
    twoParameterOperation(req, res, (a, b) => a * b);
}

function division(req, res){
    twoParameterOperation(req, res, (a, b) => a / b);
}

function twoParameterOperation(req, res, operation){
    let a = Number(req.params.a);
    let b = Number(req.params.b);
    res.json(operation(a,b));
}


module.exports = {sum, substraction, division, multiplication}